import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TestOverviewComponent} from './TEST/test-overview.component';
import {PersonService} from './concepts/person/domain/person.service';
import {TripService} from './concepts/trip/domain/trip.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToolbarComponent} from './shared/commons/toolbar.component';
import {TripOverviewComponent} from './concepts/trip/read/trip-overview.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CreateTripModalComponent} from './concepts/trip/write/create-trip-modal.component';
import {BalanceService} from './concepts/balance/domain/balance.service';
import {TripDetailComponent} from './concepts/trip/read/trip-detail.component';
import {CreateExpenseModalComponent} from './concepts/expense/write/create-expense-modal.component';
import {ExpenseService} from './concepts/expense/domain/expense.service';
import {ExpenseTableComponent} from './concepts/expense/read/expense-table.component';
import {PersonOverviewComponent} from './concepts/person/read/person-overview.component';
import {PersonInformationCardComponent} from './concepts/person/read/person-information-card.component';
import {CreatePersonModalComponent} from './concepts/person/write/create-person-modal.component';
import {ModalComponent} from './shared/commons/modal.component';
import {TestFormComponent} from './TEST/test-form.component';
import {ExampleComponent} from './TEST/example.component';
import {TestCreatePersonModalComponent} from './TEST/test-create-person-modal.component';
import {TestCreateExpenseModalComponent} from './TEST/test-create-expense-modal.component';
import {TextMaskModule} from 'angular2-text-mask';
import {EuroCurrencyPipe} from './infrastructure/util/amount/euro-currency.pipe';
import {SignUpComponent} from './security/sign-up.component';
import {LoginComponent} from './security/login.component';
import {AuthService} from './security/auth/auth.service';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {httpInterceptorProviders} from './security/auth/auth-interceptor';
import {TokenStorageService} from './security/auth/token-storage.service';
import {AppMaterialModule} from './app-material/app-material.module';
import {LoginSucceededComponent} from './home/login-succeeded.component';
import {AuthenticationGuard} from './security/auth/authentication.guard';
import {SignUpMatComponent} from './security/sign-up-mat.component';
import {SidebarComponent} from './shared/commons/sidebar.component';
import {SubToolbarComponent} from './shared/commons/sub-toolbar.component';
import {UserHomeComponent} from './home/user-home.component';
import {PersonBalanceComponent} from './concepts/balance/read/person-balance.component';
import {TitleComponent} from './shared/commons/breadcrumb.component';
import {TitleService} from './shared/commons/title.service';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {BalancesChartComponent} from './home/balances-chart.component';
import {TripOverviewChartsComponent} from './concepts/trip/read/trip-overview-charts.component';
import {ChartsModule} from 'ng2-charts';
import {EditPersonModalComponent} from './concepts/person/write/edit-person-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateExpenseModalComponent,
    CreatePersonModalComponent,
    CreateTripModalComponent,
    ExpenseTableComponent,
    EditPersonModalComponent,
    LoginComponent,
    LoginSucceededComponent,
    ModalComponent,
    PersonBalanceComponent,
    PersonInformationCardComponent,
    PersonOverviewComponent,
    SidebarComponent,
    SignUpComponent,
    SignUpMatComponent,
    SubToolbarComponent,
    BalancesChartComponent,
    TestOverviewComponent,
    TitleComponent,
    ToolbarComponent,
    TripDetailComponent,
    TripOverviewComponent,
    TripOverviewChartsComponent,
    UserHomeComponent,
    // PIPES
    EuroCurrencyPipe,
    // TEST
    LoginComponent,
    TestFormComponent,
    ExampleComponent,
    TestCreateExpenseModalComponent,
    TestCreatePersonModalComponent
  ],
  entryComponents: [
    CreateExpenseModalComponent,
    CreatePersonModalComponent,
    CreateTripModalComponent,
    EditPersonModalComponent,
    TestCreateExpenseModalComponent,
    TestCreatePersonModalComponent
  ],
  imports: [
    AppMaterialModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    ChartsModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    NgxChartsModule,
    ReactiveFormsModule,
    TextMaskModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-full-width',
      preventDuplicates: true
    })
  ],
  providers: [
    AuthenticationGuard,
    AuthService,
    BalanceService,
    ExpenseService,
    httpInterceptorProviders,
    PersonService,
    TitleService,
    TokenStorageService,
    TripService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
