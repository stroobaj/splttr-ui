import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators} from '@angular/forms';
import {AuthService} from './auth/auth.service';
import {SignUpInfo} from './auth/sign-up-info';
import {ToastrService} from 'ngx-toastr';
import {ResponseMessage} from './response/response-message';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-up-mat',
  styleUrls: ['./sign-up-mat.component.scss'],
  template: `
    <div class="signin-content">
      <mat-card>
        <mat-card-content>
          <form [formGroup]="signUpForm"
                (ngSubmit)="signUp()">

            <h4>SPLTTR</h4>
            <p>Sign up</p>

            <mat-form-field class="full-width-input">
              <input matInput
                     placeholder="First Name"
                     formControlName="firstName"
                     required>
              <mat-error *ngIf="isFieldInvalid('firstName')">First name is required</mat-error>
            </mat-form-field>

            <mat-form-field class="full-width-input">
              <input matInput
                     placeholder="Last Name"
                     formControlName="lastName"
                     required>
              <mat-error *ngIf="isFieldInvalid('lastName')">Last name is required</mat-error>
            </mat-form-field>

            <mat-form-field class="full-width-input">
              <input matInput
                     placeholder="Email"
                     formControlName="email"
                     required>
              <mat-error *ngIf="isFieldInvalid('email')">Email is required</mat-error>
            </mat-form-field>

            <mat-form-field class="full-width-input">
              <input matInput
                     placeholder="Username"
                     formControlName="username"
                     required>
              <mat-error *ngIf="isFieldInvalid('username')">Username is required</mat-error>
            </mat-form-field>

            <mat-form-field class="full-width-input">
              <input matInput
                     type="password"
                     placeholder="Password"
                     formControlName="password"
                     required>
              <mat-error *ngIf="isFieldInvalid('password')">Password is required</mat-error>
            </mat-form-field>

            <button mat-raised-button
                    color="primary"
                    type="submit">Sign Up
              <!--[disabled]="!loginForm.valid">Login-->
            </button>

          </form>
        </mat-card-content>
      </mat-card>
    </div>
  `
})
export class SignUpMatComponent implements OnInit {

  signUpForm: FormGroup;
  firstName: FormControl;
  lastName: FormControl;
  email: FormControl;
  username: FormControl;
  password: FormControl;

  formSubmitAttempt: boolean = false;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor(private authService: AuthService,
              private router: Router,
              private formBuilder: FormBuilder,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.createFormControls();
    this.createForm();
  }

  createFormControls = (): void => {
    this.firstName = this.formBuilder.control('', Validators.required);
    this.lastName = this.formBuilder.control('', Validators.required);
    this.email = this.formBuilder.control('', Validators.required);
    this.username = this.formBuilder.control('', Validators.required);
    this.password = this.formBuilder.control('', Validators.required);
  };

  createForm = (): void => {
    this.signUpForm = this.formBuilder.group({
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      username: this.username,
      password: this.password,
    });
  };

  signUp = (): void => {
    if (this.signUpForm.valid) {
      const signUpInfo: SignUpInfo = JSON.parse(JSON.stringify(this.signUpForm.value));

      this.authService.signUp(signUpInfo).subscribe(
        (responseMessage: ResponseMessage) => {
          console.log(responseMessage);
          this.toastr.success(responseMessage.message);
          this.formSubmitAttempt = false;
          this.formGroupDirective.resetForm(); // See README issue 1.
          this.router.navigate(['login']);
        },
        error => {
          this.toastr.error(error.error.message);
        });
    }
    this.formSubmitAttempt = true;
  };

  isFieldInvalid = (field: string): boolean => {
    return ((!this.signUpForm.get(field).valid && this.signUpForm.get(field).touched) ||
      (this.signUpForm.get(field).untouched && this.formSubmitAttempt));
  };

}
