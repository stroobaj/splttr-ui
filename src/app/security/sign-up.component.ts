import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from './auth/auth.service';
import {SignUpInfo} from './auth/sign-up-info';
import {ToastrService} from 'ngx-toastr';
import {ResponseMessage} from './response/response-message';

@Component({
  selector: 'app-sign-up',
  template: `
    <div class="container">

      <form [formGroup]="signUpForm"
            (ngSubmit)="signUp()"
            novalidate>

        <div class="form-group">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">First Name</span>
            </div>
            <input type="text"
                   class="form-control"
                   formControlName="firstName"
                   required
                   [ngClass]="{'is-invalid': firstName.invalid && (firstName.dirty || firstName.touched),
                               'is-valid': firstName.valid && (firstName.dirty || firstName.touched)}">
            <div *ngIf="firstName.errors && (firstName.dirty || firstName.touched)"
                 class="invalid-feedback">
              <div *ngIf="firstName.errors['required']">First Name is required</div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">Last Name</span>
            </div>
            <input type="text"
                   class="form-control"
                   formControlName="lastName"
                   required
                   [ngClass]="{'is-invalid': lastName.invalid && (lastName.dirty || lastName.touched),
                               'is-valid': lastName.valid && (lastName.dirty || lastName.touched)}">
            <div *ngIf="lastName.errors && (lastName.dirty || lastName.touched)"
                 class="invalid-feedback">
              <div *ngIf="lastName.errors['required']">Last Name is required</div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">Email</span>
            </div>
            <input type="text"
                   class="form-control"
                   formControlName="email"
                   required
                   [ngClass]="{'is-invalid': email.invalid && (email.dirty || email.touched),
                               'is-valid': email.valid && (email.dirty || email.touched)}">
            <div *ngIf="email.errors && (email.dirty || email.touched)"
                 class="invalid-feedback">
              <div *ngIf="email.errors['required']">Email is required</div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">Username</span>
            </div>
            <input type="text"
                   class="form-control"
                   formControlName="username"
                   required
                   [ngClass]="{'is-invalid': username.invalid && (username.dirty || username.touched),
                               'is-valid': username.valid && (username.dirty || username.touched)}">
            <div *ngIf="username.errors && (username.dirty || username.touched)"
                 class="invalid-feedback">
              <div *ngIf="username.errors['required']">Username is required</div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">Password</span>
            </div>
            <input type="text"
                   class="form-control"
                   formControlName="password"
                   required
                   [ngClass]="{'is-invalid': password.invalid && (password.dirty || password.touched),
                               'is-valid': password.valid && (password.dirty || password.touched)}">
            <div *ngIf="password.errors && (password.dirty || password.touched)"
                 class="invalid-feedback">
              <div *ngIf="password.errors['required']">Password is required</div>
            </div>
          </div>
        </div>

        <button type="submit"
                class="btn btn-primary"
                [disabled]="!signUpForm.valid">Submit
        </button>

      </form>

    </div>
  `
})
export class SignUpComponent implements OnInit {

  signUpForm: FormGroup;
  firstName: FormControl;
  lastName: FormControl;
  email: FormControl;
  username: FormControl;
  password: FormControl;

  constructor(private authService: AuthService,
              private formBuilder: FormBuilder,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.createFormControls();
    this.createForm();
  }

  createFormControls = (): void => {
    this.firstName = this.formBuilder.control('', Validators.required);
    this.lastName = this.formBuilder.control('', Validators.required);
    this.email = this.formBuilder.control('', Validators.required);
    this.username = this.formBuilder.control('', Validators.required);
    this.password = this.formBuilder.control('', Validators.required);
  };

  createForm = (): void => {
    this.signUpForm = this.formBuilder.group({
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      username: this.username,
      password: this.password,
    });
  };

  signUp = (): void => {
    const signUpInfo: SignUpInfo = JSON.parse(JSON.stringify(this.signUpForm.value));

    this.authService.signUp(signUpInfo).subscribe(
      (responseMessage: ResponseMessage) => {
        console.log(responseMessage);
        this.toastr.success(responseMessage.message);
        this.signUpForm.reset();
      },
      error => {
        this.toastr.error(error.error.message);
      });
  };


}
