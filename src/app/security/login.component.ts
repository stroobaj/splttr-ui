import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators} from '@angular/forms';
import {TokenStorageService} from './auth/token-storage.service';
import {LoginCredentials} from './auth/login-credentials';
import {AuthService} from './auth/auth.service';
import {JwtResponse} from './auth/jwt-response';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  styleUrls: ['./login.component.scss'],
  template: `
    <div class="signin-content">
      <mat-card>
        <mat-card-content>
          <form [formGroup]="loginForm"
                (ngSubmit)="login()">

            <h4>SPLTTR</h4>
            <p>Please login to continue</p>

            <mat-form-field class="full-width-input">
              <input matInput
                     placeholder="Username"
                     formControlName="username"
                     required>
              <mat-error *ngIf="isFieldInvalid('username')">Username is required</mat-error>
            </mat-form-field>

            <mat-form-field class="full-width-input">
              <input matInput
                     type="password"
                     placeholder="Password"
                     formControlName="password"
                     required>
              <mat-error *ngIf="isFieldInvalid('password')">Password is required</mat-error>
            </mat-form-field>

            <button mat-raised-button
                    color="primary"
                    type="submit"
                    [disabled]="!loginForm.valid">Login
            </button>

            <div style="margin-top: 5px">
              <a routerLink="/signup">Create account</a>
            </div>

          </form>
        </mat-card-content>
      </mat-card>
    </div>
  `
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  username: FormControl;
  password: FormControl;

  isLoggedIn: boolean = false;
  formSubmitAttempt: boolean;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor(private tokenStorage: TokenStorageService,
              private authService: AuthService,
              private formBuilder: FormBuilder,
              private router: Router,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
    }
    this.createFormControls();
    this.createForm();
  }

  createFormControls = (): void => {
    this.username = this.formBuilder.control('', Validators.required);
    this.password = this.formBuilder.control('', Validators.required);
  };

  createForm = (): void => {
    this.loginForm = this.formBuilder.group({
      username: this.username,
      password: this.password,
    });
  };

  login = (): void => {
    if (this.loginForm.valid) {
      const loginCredentials: LoginCredentials = JSON.parse(JSON.stringify(this.loginForm.value));

      this.authService.login(loginCredentials).subscribe(
        (jwtResponse: JwtResponse) => {
          this.tokenStorage.saveToken(jwtResponse.token);
          this.tokenStorage.saveUsername(jwtResponse.username);
          this.tokenStorage.saveAuthorities(jwtResponse.authorities);

          this.isLoggedIn = true;
          this.formSubmitAttempt = false;
          this.formGroupDirective.resetForm(); // See README issue 1.

          this.router.navigate(['home']);
        },
        error => {
          console.log('login error:', error);
          this.toastr.error(error.error.message);
        }
      );
    }
    this.formSubmitAttempt = true;
  };

  isFieldInvalid = (field: string): boolean => {
    return (
      (!this.loginForm.get(field).valid && this.loginForm.get(field).touched) ||
      (this.loginForm.get(field).untouched && this.formSubmitAttempt)
    );
  };

}
