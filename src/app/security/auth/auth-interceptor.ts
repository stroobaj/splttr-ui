import {HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';

import {TokenStorageService} from './token-storage.service';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

const TOKEN_HEADER_KEY = 'Authorization';

// TODO: check if we provide new/updated token with every request
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private tokenStorageService: TokenStorageService,
              private toastr: ToastrService,
              private router: Router) {
  }

  intercept(request: HttpRequest<any>, handler: HttpHandler): Observable<HttpEvent<any>> {
    let authReq = request;
    const token = this.tokenStorageService.getToken();
    if (token != null) {
      authReq = request.clone({
        headers: request.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token)
      });
    }

    return handler.handle(authReq).pipe(
      catchError(err => {
        let errorMessage: string;
        console.log('AuthInterceptor', err);
        if (err.status === 403) {
          errorMessage = 'Acces denied: token expired.';
        } else {
          errorMessage = err.error.message;
        }
        this.redirectToLogin();
        this.toastr.error(errorMessage);
        return of(err);
      })
    );
  }

  redirectToLogin = (): void => {
    console.log('AuthInterceptor.redirectToLogin');
    this.tokenStorageService.signOut();
    this.router.navigate(['login']);
  };

}


export const httpInterceptorProviders = [
  {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
];
