import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {SignUpInfo} from './sign-up-info';
import {ResponseMessage} from '../response/response-message';
import {LoginCredentials} from './login-credentials';
import {JwtResponse} from './jwt-response';
import {TokenStorageService} from './token-storage.service';
import {JwtHelperService} from '@auth0/angular-jwt';

const jwtHelper = new JwtHelperService();
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable(
  // TODO: check use
  // {providedIn: 'root'}
)
export class AuthService {

  private SIGN_UP_URL: string = '/api/auth/signup';
  private LOGIN_URL: string = '/api/auth/login';

  constructor(private http: HttpClient,
              private tokenStorage: TokenStorageService) {
  }

  signUp(signUpInfo: SignUpInfo): Observable<ResponseMessage> {
    return this.http.post<ResponseMessage>(this.SIGN_UP_URL, signUpInfo, httpOptions);
  }

  login(credentials: LoginCredentials): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.LOGIN_URL, credentials, httpOptions);
  }

  public signOut = (): void => {
    this.tokenStorage.signOut();
  };

  public isAuthenticated = (): boolean => {
    // TODO: Check whether the token is expired and return true or false > Only do this in the backend?
    if (this.tokenPresentAndNotExpired()) {
      console.log('TOKEN PRESENT');
      return true;
    }
    console.log('NO TOKEN PRESENT OR TOKEN EXPIRED');
    return false;
  };

  public getPrincipalId = (): string => {
    if (this.tokenStorage.getToken() != null) {
      return jwtHelper.decodeToken(this.tokenStorage.getToken()).userId;
    }
    return '';
  };

  private tokenPresentAndNotExpired = (): boolean => {
    if (this.tokenStorage.getToken() != null) {
      return !jwtHelper.isTokenExpired(this.tokenStorage.getToken());
    }
    return false;
  };

}
