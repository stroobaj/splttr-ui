import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from './auth.service';

@Injectable()
export class AuthenticationGuard implements CanActivate {

  constructor(public authenticationService: AuthService,
              public router: Router) {
  }

  public canActivate(): boolean {
    if (this.authenticationService.isAuthenticated()) {
      console.log('AuthenticationGuard: is authenticated');
      return true;
    } else {
      console.log('AuthenticationGuard: is not Authenticated, redirect to login');
      // TODO: Don't clear sessionStorage when user is not authenticated to check error message when trying to log in with invalid token.
      this.authenticationService.signOut();
      this.redirectToLogin();
      return false;
    }
  }

  redirectToLogin = (): void => {
    this.router.navigate(['login']);
  };

}

