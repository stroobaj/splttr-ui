export interface SignUpInfo {
  firstName: string;
  lastName: string;
  email: string;
  username: string;
  password: string;
}
