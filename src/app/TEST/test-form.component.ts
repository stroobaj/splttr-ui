import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-test-form',
  template: `
    <form novalidate [formGroup]="myForm" (ngSubmit)="onSubmit()">

      <div class="form-group">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="first-name">First Name</span>
          </div>
          <input type="text"
                 class="form-control"
                 formControlName="firstName"
                 required
                 [ngClass]="{'is-invalid': firstName.invalid && (firstName.dirty || firstName.touched),
                           'is-valid': firstName.valid && (firstName.dirty || firstName.touched)}">
          <div *ngIf="firstName.errors && (firstName.dirty || firstName.touched)"
               class="invalid-feedback">
            <span *ngIf="firstName.errors['required']">First Name is required</span>
          </div>
        </div>
      </div>

      <div class="form-group">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text">Last Name</span>
          </div>
          <input type="text"
                 class="form-control"
                 formControlName="lastName"
                 required
                 [ngClass]="{'is-invalid': lastName.invalid && (lastName.dirty || lastName.touched),
                           'is-valid': lastName.valid && (lastName.dirty || lastName.touched)}">
          <div *ngIf="lastName.errors && (lastName.dirty || lastName.touched)"
               class="invalid-feedback">
            <span *ngIf="lastName.errors['required']">Last Name is required</span>
          </div>
        </div>
      </div>

      <div class="form-group">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text">Email</span>
          </div>
          <input type="text"
                 class="form-control"
                 formControlName="email"
                 required
                 [ngClass]="{'is-invalid': email.invalid && (email.dirty || email.touched),
                           'is-valid': email.valid && (email.dirty || email.touched)}">
          <div *ngIf="email.errors && (email.dirty || email.touched)"
               class="invalid-feedback">
            <span *ngIf="email.errors['required']">Email is required</span>
          </div>
        </div>
      </div>

      <div class="form-group">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text">Phone</span>
          </div>
          <input type="text"
                 class="form-control"
                 formControlName="phone"
                 required
                 [ngClass]="{'is-invalid': phone.invalid && (phone.dirty || phone.touched),
                           'is-valid': phone.valid && (phone.dirty || phone.touched)}">
          <div *ngIf="phone.errors && (phone.dirty || phone.touched)"
               class="invalid-feedback">
            <span *ngIf="phone.errors['required']">Phone is required</span>
          </div>
        </div>
      </div>
      

      <button type="submit" [disabled]="!myForm.valid">Submit</button>

    </form>

    <pre>{{myForm.value | json}}</pre>
  `
})

export class TestFormComponent implements OnInit {

  myForm: FormGroup;
  firstName: FormControl;
  lastName: FormControl;
  email: FormControl;
  phone: FormControl;

  ngOnInit(): void {
    this.createFormControls();
    this.createForm();
  }

  createFormControls = (): void => {
    this.firstName = new FormControl('', Validators.required);
    this.lastName = new FormControl('', Validators.required);
    this.email = new FormControl('', [
      Validators.required
      // Validators.pattern('[^ @]*@[^ @]*')
    ]);
    this.phone = new FormControl('', Validators.required);
  };

  createForm = (): void => {
    this.myForm = new FormGroup({
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      phone: this.phone,
    });
  };

  onSubmit = (): void => {
    console.warn(this.myForm.value);
  };

}
