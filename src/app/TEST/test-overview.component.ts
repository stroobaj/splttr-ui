import {Component, OnInit} from '@angular/core';
import {Person} from '../concepts/person/domain/person';
import {PersonService} from '../concepts/person/domain/person.service';
import {FullName, getDisplayName} from '../infrastructure/util/full-name/full-name';
import {PersonInfo} from '../concepts/person/domain/person-info';
import {newPersonInfo} from '../concepts/person/domain/person.factory';
import {CreateTrip} from '../concepts/trip/domain/create-trip';
import {newCreateTrip} from '../concepts/trip/domain/trip.factory';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CreateExpenseModalComponent} from '../concepts/expense/write/create-expense-modal.component';

@Component({
  selector: 'app-test-overview',
  template: `
    <div class="container">

      <div class="row">
        <h3>TEST OVERVIEW</h3>
      </div>

      <button type="button"
              class="btn btn-outline-primary app-create-button"
              (click)="open()">Open test modal
      </button>

      <app-test-form></app-test-form>

      <app-example></app-example>

      <div class="row">
        <h5>CREATE PERSON</h5>
        <form #createPersonForm="ngForm" (ngSubmit)="createPerson(personToCreate)">
          <input [(ngModel)]="personToCreate.firstName" name="first" placeholder="Firstname">
          <input [(ngModel)]="personToCreate.lastName" name="last" placeholder="Lastname">
          <button>Submit</button>
        </form>
      </div>

      <div class="row">
        <h5>CREATE TRIP</h5>
        <form #createTripForm="ngForm" (ngSubmit)="createTrip(createTripForm)">
          <input [(ngModel)]="tripToCreate.name" name="name" placeholder="Name">
          <button>Submit</button>
        </form>
      </div>

      <div class="row">
        <div class="form-check" *ngFor="let person of persons; let i = index">
          <input class="form-check-input" type="checkbox" value="" id="defaultCheck-{{i}}" (change)="change($event, person.personId)">
          <label class="form-check-label" for="defaultCheck-{{i}}">{{getName(person.fullName)}}</label>
        </div>
      </div>

      <div class="row">
        <table>
          <thead>
          <tr>
            <th>PERSON ID</th>
            <th>PERSON FULLNAME</th>
          </tr>
          </thead>
          <tbody>
          <tr *ngFor="let person of persons">
            <td>{{person.personId}}</td>
            <td>{{getName(person.fullName)}}</td>
          </tr>
          </tbody>
        </table>
      </div>

    </div>
  `
})

export class TestOverviewComponent implements OnInit {

  persons: Array<Person>;
  personToCreate: PersonInfo = newPersonInfo();
  tripToCreate: CreateTrip = newCreateTrip();

  constructor(private personService: PersonService,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.getPersonData();
  }

  createPerson = (createPerson: PersonInfo): void => {
    this.personService.createPerson(createPerson);
  };

  createTrip = (value: any): void => {
    console.log(value);
  };

  getPersonData = (): void => {
    this.personService.getAllPersons().subscribe((persons: Person[]) => {
      this.persons = persons;
    });
  };

  getName = (fullName: FullName): string => {
    return getDisplayName(fullName);
  };

  change = (event: Event, personId: string): void => {
    const index = this.tripToCreate.persons.indexOf(personId);
    console.log(index);

    if (index >= 0) {
      this.tripToCreate.persons.splice(index, 1);
      console.log('splice');
    } else {
      console.log('push');
      this.tripToCreate.persons.push(personId);
    }


    console.log(this.tripToCreate.persons);
  };


  open = (): void => {
    const ngbModalRef = this.modalService.open(CreateExpenseModalComponent);
    ngbModalRef.componentInstance.title = 'TEST TITLE';
  };

}


