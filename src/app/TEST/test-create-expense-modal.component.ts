import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {CreateExpense} from '../concepts/expense/domain/create-expense';
import {ExpenseService} from '../concepts/expense/domain/expense.service';
import {ActivatedRoute} from '@angular/router';
import {TripOverview} from '../concepts/trip/domain/trip-overview';
import {FullName, getDisplayName} from '../infrastructure/util/full-name/full-name';

@Component({
  selector: 'app-test-create-expense-modal',
  template: `
    <app-modal [title]="'Create Expense'"
               [hasClose]="true">

      <form [formGroup]="createExpenseForm"
            (ngSubmit)="createExpense()"
            novalidate>

        <div class="form-group">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">Description</span>
            </div>
            <input type="text"
                   class="form-control"
                   formControlName="description"
                   required
                   [ngClass]="{'is-invalid': description.invalid && (description.dirty || description.touched),
                               'is-valid': description.valid && (description.dirty || description.touched)}">
            <div *ngIf="description.errors && (description.dirty || description.touched)"
                 class="invalid-feedback">
              <span *ngIf="description.errors['required']">Description is required</span>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">Amount</span>
            </div>
            <input type="text"
                   class="form-control"
                   formControlName="amount"
                   required
                   [ngClass]="{'is-invalid': amount.invalid && (amount.dirty || amount.touched),
                               'is-valid': amount.valid && (amount.dirty || amount.touched)}">
            <div *ngIf="amount.errors && (amount.dirty || amount.touched)"
                 class="invalid-feedback">
              <span *ngIf="amount.errors['required']">Amount is required</span>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">Date</span>
            </div>
            <input type="text"
                   class="form-control"
                   formControlName="date"
                   required
                   [ngClass]="{'is-invalid': date.invalid && (date.dirty || date.touched),
                               'is-valid': date.valid && (date.dirty || date.touched)}">
            <div *ngIf="date.errors && (date.dirty || date.touched)"
                 class="invalid-feedback">
              <span *ngIf="date.errors['required']">Date is required</span>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">Paid by</span>
            </div>
            <select class="custom-select"
                    formControlName="payer"
                    required
                    [ngClass]="{'is-invalid': payer.invalid && (payer.dirty || payer.touched),
                               'is-valid': payer.valid && (payer.dirty || payer.touched)}">>
              <option *ngFor="let person of trip.persons"
                      [ngValue]="person.personId">{{getPersonDisplayName(person.fullName)}}
              </option>
              <div *ngIf="date.errors && (date.dirty || date.touched)"
                   class="invalid-feedback">
                <span *ngIf="date.errors['required']">Payer is required</span>
              </div>
            </select>
          </div>
        </div>

        <button type="submit"
                class="btn btn-primary"
                [disabled]="!createExpenseForm.valid">Submit
        </button>

      </form>

    </app-modal>
  `
})

export class TestCreateExpenseModalComponent implements OnInit {

  // TODO: check order of class properties / fields.
  @Output() onExpenseCreated: EventEmitter<any> = new EventEmitter<any>();

  trip: TripOverview;

  createExpenseForm: FormGroup;
  description: FormControl;
  amount: FormControl;
  date: FormControl;
  payer: FormControl;

  constructor(private expenseService: ExpenseService,
              private ngbActiveModal: NgbActiveModal,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.createFormControls();
    this.createForm();
  }

  createFormControls = (): void => {
    this.description = this.formBuilder.control('', Validators.required);
    this.amount = this.formBuilder.control('', Validators.required);
    this.date = this.formBuilder.control('', Validators.required);
    this.payer = this.formBuilder.control('', Validators.required);
  };

  createForm = (): void => {
    this.createExpenseForm = this.formBuilder.group({
      description: this.description,
      amount: this.amount,
      date: this.date,
      payer: this.payer,
    });
  };

  createExpense = (): void => {
    const createExpense: CreateExpense = JSON.parse(JSON.stringify(this.createExpenseForm.value));
    console.log('CREATE EXPENSE', createExpense);
    // this.expenseService.createExpense(createExpense).subscribe(() => {
    //   this.onExpenseCreated.emit();
    // });
    this.closeModal();
  };

  getPersonDisplayName = (fullName: FullName): string => {
    return getDisplayName(fullName);
  };

  closeModal = (): void => {
    this.ngbActiveModal.close();
  };

}
