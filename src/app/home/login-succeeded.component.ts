import {Component} from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <!--<div class="grid-container">-->
      <app-toolbar></app-toolbar>
      <app-sidebar></app-sidebar>
      <router-outlet></router-outlet>
    <!--</div>-->
  `
})
export class LoginSucceededComponent {
}
