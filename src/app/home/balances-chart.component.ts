import {Component, OnInit} from '@angular/core';
import {UserBalance} from '../concepts/balance/domain/person-balance';
import {BalanceService} from '../concepts/balance/domain/balance.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-balances-chart',
  template: `
    <mat-card>
      <mat-card-title>Last edited trips</mat-card-title>
      <div style="height: 300px;">
        <ngx-charts-bar-vertical [results]="viewUserBalances"
                                 [customColors]="viewUserBalancesColor"
                                 [gradient]="gradient"
                                 [xAxis]="showXAxis"
                                 [yAxis]="showYAxis"
                                 [legend]="showLegend"
                                 [showXAxisLabel]="showXAxisLabel"
                                 [showYAxisLabel]="showYAxisLabel"
                                 [xAxisLabel]="xAxisLabel"
                                 [yAxisLabel]="yAxisLabel"
                                 (select)="onSelect($event)">
        </ngx-charts-bar-vertical>
      </div>
    </mat-card>
  `
})
export class BalancesChartComponent implements OnInit {

  viewUserBalances: any[] = [];
  viewUserBalancesColor: any[] = [];
  userBalances: UserBalance[];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Trip';
  showYAxisLabel = true;
  yAxisLabel = 'Balance';

  constructor(private balanceService: BalanceService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getBalances();
  }

  getBalances = (): void => {
    this.balanceService.getLastEditedUserBalances().subscribe((userBalances: UserBalance[]) => {
        this.userBalances = userBalances;
        this.userBalances.map(userBalance => {
          this.viewUserBalances.push({name: userBalance.tripName, value: userBalance.balance, extra: userBalance.tripId});
          this.assignColors(userBalance);
        });
        this.viewUserBalances = [...this.viewUserBalances];
        this.viewUserBalancesColor = [...this.viewUserBalancesColor];
        Object.assign(this, this.viewUserBalances);
      }
    );
  };

  private assignColors = (userBalance: UserBalance): void => {
    let color: string = '';

    if (this.isPositiveBalance(userBalance.balance)) {
      color = '#52EB7C';
    } else {
      color = '#9E0F11';
    }

    this.viewUserBalancesColor.push({name: userBalance.tripName, value: color});
  };

  private isPositiveBalance = (balance: string): boolean => {
    return Math.sign(parseFloat(balance)) >= 0;
  };

  onSelect(event) {
    this.router.navigate(['trip', event.extra]);
  }

}
