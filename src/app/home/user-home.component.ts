import {Component, OnInit} from '@angular/core';
import {TitleService} from '../shared/commons/title.service';
import {TokenStorageService} from '../security/auth/token-storage.service';

@Component({
  selector: 'app-user-home',
  template: `
    <!--<div class="container-fluid">-->
    <div class="container">
      <div style="margin-top: 20px">Welcome to SPLTTR, {{user}}.</div>
      <div>
        <app-balances-chart></app-balances-chart>
      </div>
    </div>
  `
})
export class UserHomeComponent implements OnInit {

  constructor(private titleService: TitleService,
              private tokenStorageService: TokenStorageService) {
  }

  user: string;

  ngOnInit(): void {
    this.titleService.setTitle(['HOME']);
    this.user = this.tokenStorageService.getUsername();
  }

}
