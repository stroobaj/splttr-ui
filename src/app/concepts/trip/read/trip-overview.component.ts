import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TripService} from '../domain/trip.service';
import {TripOverview} from '../domain/trip-overview';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {RunningRequests} from '../../../shared/commons/domain/running-requests';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CreateTripModalComponent} from '../write/create-trip-modal.component';
import {TitleService} from '../../../shared/commons/title.service';

@Component({
  selector: 'app-trip-overview',
  styleUrls: ['./trip-overview.component.scss'],
  template: `
    <div class="container">
      <!-- MODAL -->
      <button #createTripButton
              mat-raised-button
              type="button"
              class="app-create-button"
              (click)="openCreateTripModal()">Create Trip
      </button>

      <!-- ACCORDION -->
      <div *ngIf="!runningRequests.isRequestForAllTripsRunning()">
        <div *ngIf="!trips || trips.length === 0">
          No trips found.
        </div>

        <mat-accordion *ngIf="trips && trips.length > 0"
                       class="app-accordion">

          <mat-expansion-panel *ngFor="let trip of trips" class="mat-elevation-z3">
            <mat-expansion-panel-header>{{getTripTitle(trip)}}</mat-expansion-panel-header>
            <div class="accordion-content">
              <div class="accordion-balances">
                <app-person-balance *ngFor="let tripBalance of trip.balances"
                                    [personBalance]="tripBalance">
                </app-person-balance>
              </div>
              <div>
                <button mat-raised-button
                        type="button"
                        class="float-right"
                        (click)="openTrip(trip.tripId)">Open Trip
                </button>
              </div>
            </div>
            <app-trip-overview-charts *ngIf="trip.balances"
                                      [personBalances]="trip.balances">
            </app-trip-overview-charts>
          </mat-expansion-panel>
        </mat-accordion>

      </div>
    </div>
  `
})

export class TripOverviewComponent implements OnInit, OnDestroy {

  @ViewChild('createTripButton')
  private createTripButton: ElementRef;

  trips: TripOverview[] = [];
  subscription: Subscription;
  runningRequests: RunningRequests = new RunningRequests();

  constructor(private tripService: TripService,
              private modalService: NgbModal,
              private router: Router,
              private titleService: TitleService) {
  }

  ngOnInit(): void {
    this.loadTrips();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  loadTrips = (): void => {
    this.runningRequests.startRequestForTrips();
    this.subscription = this.tripService.getAllTrips().subscribe((trips) => {
        this.trips = trips;
        this.titleService.setTitle(['TRIPS']);
      },
      (error) => console.log('Error: ', error),
      () => this.runningRequests.allTripsRetrieved());
  };

  getTripTitle = (trip: TripOverview): string => {
    return trip.name;
  };

  openTrip = (tripId: string): void => {
    this.router.navigate(['trip', tripId]);
  };

  openCreateTripModal = (): void => {
    const ngbModalRef = this.modalService.open(CreateTripModalComponent);
    ngbModalRef.componentInstance.onTripCreated.subscribe(() => {
      this.loadTrips();
    });
    // TODO: check better option - above of below.
    ngbModalRef.result
      .then(() => {
        this.createTripButton['_elementRef'].nativeElement.classList.remove('cdk-program-focused');
        this.createTripButton['_elementRef'].nativeElement.classList.add('cdk-mouse-focused');
        console.log('SUBMIT');
      })
      .catch(() => {
        this.createTripButton['_elementRef'].nativeElement.classList.remove('cdk-program-focused');
        this.createTripButton['_elementRef'].nativeElement.classList.add('cdk-mouse-focused');
        console.log('CLOSE');
      });

  };

}
