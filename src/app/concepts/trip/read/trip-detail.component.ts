import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TripOverview} from '../domain/trip-overview';
import {ActivatedRoute} from '@angular/router';
import {TripService} from '../domain/trip.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CreateExpenseModalComponent} from '../../expense/write/create-expense-modal.component';
import {RunningRequests} from '../../../shared/commons/domain/running-requests';
import {TitleService} from '../../../shared/commons/title.service';

@Component({
  selector: 'app-trip-detail',
  styleUrls: ['./trip-detail.component.scss'],
  template: `
    <div *ngIf="this.trip"
         class="container">
      <button #createExpenseButton
              mat-raised-button
              class="app-create-button"
              type="button"
              (click)="openCreateExpenseModal()">Create Expense
      </button>

      <div *ngIf="!runningRequests.isRequestForAllExpensesRunning()">
        <div class="row">
          <div class="col-md-10">
            <app-expense-table [expenses]="trip.expenses"
                               [tripId]="trip.tripId">
            </app-expense-table>
          </div>
          <div class="col-md-2">
            <div id="person-balances"
                 *ngFor="let tripBalance of trip.balances">
              <app-person-balance [personBalance]="tripBalance">
              </app-person-balance>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
})

export class TripDetailComponent implements OnInit {

  @ViewChild('createExpenseButton')
  private createExpenseButton: ElementRef;

  trip: TripOverview;
  runningRequests: RunningRequests = new RunningRequests();

  constructor(private route: ActivatedRoute,
              private tripService: TripService,
              private modalService: NgbModal,
              private titleService: TitleService) {
  }

  ngOnInit(): void {
    this.loadTrip();
  }

  loadTrip = (): void => {
    this.runningRequests.startRequestForExpenses();
    this.route.params.subscribe(params => {
      this.tripService.getTripById(params['tripId']).subscribe(trip => {
          this.trip = trip;
          this.titleService.setTitle(['TRIPS', trip.name]);
        },
        (error) => console.log('Error: ', error),
        () => this.runningRequests.allExpensesRetrieved());
    });
  };

  openCreateExpenseModal = (): void => {
    const ngbModalRef = this.modalService.open(CreateExpenseModalComponent);
    ngbModalRef.componentInstance.trip = this.trip;
    ngbModalRef.componentInstance.onExpenseCreated.subscribe(() => {
      this.loadTrip();
    });
    // TODO: check better option - above of below.
    ngbModalRef.result
      .then(() => {
        this.createExpenseButton['_elementRef'].nativeElement.classList.remove('cdk-program-focused');
        this.createExpenseButton['_elementRef'].nativeElement.classList.add('cdk-mouse-focused');
        console.log('SUBMIT');
      })
      .catch(() => {
        this.createExpenseButton['_elementRef'].nativeElement.classList.remove('cdk-program-focused');
        this.createExpenseButton['_elementRef'].nativeElement.classList.add('cdk-mouse-focused');
        console.log('CLOSE');
      });
  };

}
