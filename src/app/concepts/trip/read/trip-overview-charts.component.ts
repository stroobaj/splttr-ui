import {Component, Input, OnInit} from '@angular/core';
import {PersonBalance} from '../../balance/domain/person-balance';

@Component({
  selector: 'app-trip-overview-charts',
  template: `
    <div style="padding-top: 20px">
      <ngx-charts-bar-horizontal-2d [view]="view"
                                    [scheme]="colorScheme"
                                    [results]="viewPersonBalances"
                                    [gradient]="gradient"
                                    [xAxis]="showXAxis"
                                    [yAxis]="showYAxis"
                                    [legend]="showLegend"
                                    (select)="onSelect($event)">
      </ngx-charts-bar-horizontal-2d>
    </div>
  `
})

export class TripOverviewChartsComponent implements OnInit {

  @Input()
  personBalances: PersonBalance[] = [];

  viewPersonBalances: any[] = [];

  view: any[] = [600, 250];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;

  colorScheme = {
    domain: ['#52EB7C', '#9E0F11']
  };

  constructor() {
  }

  ngOnInit(): void {
    this.setUpBalances();
  }

  setUpBalances = (): void => {
    this.personBalances.map(personBalance => {
      this.viewPersonBalances.push({
        name: personBalance.person.fullName.firstName,
        series: [
          {
            name: 'Prepaid',
            value: personBalance.advance
          },
          {
            name: 'Borrowed',
            value: personBalance.borrowed
          }]
      });
    });
    this.viewPersonBalances = [...this.viewPersonBalances];
    Object.assign(this, this.viewPersonBalances);
  };

  onSelect(event) {
    console.log(event);
  }

}
