import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {isNullOrUndefined} from 'util';
import {Person} from '../../person/domain/person';
import {PersonService} from '../../person/domain/person.service';
import {TripService} from '../domain/trip.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {CreateTrip} from '../domain/create-trip';
import {FullName, getDisplayName} from '../../../infrastructure/util/full-name/full-name';

@Component({
  selector: 'app-create-trip-modal',
  template: `

    <app-modal [title]="'Create Trip'"
               [hasClose]="true">

      <!-- FORM -->
      <form [formGroup]="createTripForm"
            (ngSubmit)="createTrip()">

        <mat-form-field class="full-width-input">
          <input matInput
                 placeholder="Name"
                 formControlName="name"
                 required>
          <mat-error *ngIf="name.invalid && (name.dirty || name.touched)">Name is required</mat-error>
        </mat-form-field>

        <!-- CHECKBOXES PERSONS -->
        <div formArrayName="persons"
             *ngFor="let person of createTripForm.controls['persons'].controls; let i = index">
          <mat-checkbox type="checkbox"
                        [formControlName]="i">
            {{getPersonDisplayName(allPersons[i]?.fullName)}}
          </mat-checkbox>
        </div>
        <div class="app-invalid-feedback" *ngIf="createTripForm.controls['persons'].invalid 
          && (createTripForm.controls['persons'].dirty || createTripForm.controls['persons'].touched)">
          <span *ngIf="createTripForm.controls['persons'].invalid">At least one person must be selected</span>
        </div>

        <!-- SUBMIT -->
        <button mat-raised-button
                type="submit"
                [disabled]="!createTripForm.valid">Create Trip
        </button>

      </form>

    </app-modal>
  `
})
export class CreateTripModalComponent implements OnInit {

  createTripForm: FormGroup = new FormGroup({});
  name: FormControl = new FormControl('', Validators.required);
  persons: FormArray = new FormArray([]);

  allPersons: Array<Person> = [];

  @Output() onTripCreated: EventEmitter<any> = new EventEmitter<any>();

  constructor(private personService: PersonService,
              private tripService: TripService,
              private ngbActiveModal: NgbActiveModal,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.createFormControls();
    this.getAllPersons();
  }

  createFormControls = (): void => {
    this.createTripForm = this.formBuilder.group({
      name: this.name,
      persons: this.persons
    });
  };

  createTrip = (): void => {
    const selectedPersonIds: string[] = this.createTripForm.value.persons
      .map((v, i) => v ? this.allPersons[i].personId : null)
      .filter(v => v !== null);
    const createTrip: CreateTrip = JSON.parse(JSON.stringify(this.createTripForm.value));

    createTrip.persons = selectedPersonIds;

    this.tripService.createTrip(createTrip).subscribe(() => {
      this.onTripCreated.emit();
    });
    this.closeModal();
  };

  getAllPersons = (): void => {
    this.personService.getAllPersons().subscribe((allPersons: Person[]) => {
      this.allPersons = allPersons;
      this.updateFormValues();
    });
  };

  updateFormValues = (): void => {
    const controls: Array<FormControl> = this.allPersons.map(c => new FormControl(false));
    this.createTripForm.setControl('persons', this.formBuilder.array(controls, minSelectedCheckboxes(1)));
  };

  getPersonDisplayName = (fullName: FullName): string => {
    if (!isNullOrUndefined(fullName)) {
      return getDisplayName(fullName);
    }
  };

  closeModal = (): void => {
    this.ngbActiveModal.close();
  };

}

function minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      .map(control => control.value)
      .reduce((prev, next) => next ? prev + next : prev, 0);

    return totalSelected >= min ? null : {required: true};
  };
  return validator;
}
