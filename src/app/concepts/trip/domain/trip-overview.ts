import {Person} from '../../person/domain/person';
import {ExpenseInfo} from '../../expense/domain/expense-info';
import {PersonBalance} from '../../balance/domain/person-balance';

export interface TripOverview {
  tripId: string;
  name: string;
  persons: Person[];
  balances: PersonBalance[];
  expenses: ExpenseInfo[];
}
