import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TripOverview} from './trip-overview';
import {CreateTrip} from './create-trip';
import {Observable} from 'rxjs';
import {ExtendedExpenseInfo} from '../../expense/domain/expense-info';

@Injectable()
export class TripService {

  TRIP_URL: string = '/api/trip';

  constructor(private http: HttpClient) {
  }

  getAllTrips = (): Observable<Array<TripOverview>> => {
    return this.http.get<Array<TripOverview>>(this.TRIP_URL);
  };

  createTrip = (createTrip: CreateTrip): Observable<Object> => {
    return this.http.post(this.TRIP_URL, createTrip);
  };

  getTripById = (tripId: string): Observable<TripOverview> => {
    return this.http.get<TripOverview>(this.TRIP_URL + '/' + tripId);
  };

  getExtendedExpenses = (tripId: string): Observable<Array<ExtendedExpenseInfo>> => {
    return this.http.get<Array<ExtendedExpenseInfo>>(this.TRIP_URL + '/' + tripId + '/expense');
  };

}
