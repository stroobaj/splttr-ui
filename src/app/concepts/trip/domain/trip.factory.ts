import {CreateTrip} from './create-trip';

export function newCreateTrip(): CreateTrip {
  return <CreateTrip>{
    name: '',
    persons: []
  };
}
