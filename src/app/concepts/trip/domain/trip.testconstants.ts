import {TripOverview} from './trip-overview';
import {otherPerson, person} from '../../person/domain/person.testconstants';
import {anotherExpense, expense, otherExpense} from '../../expense/domain/expense.testconstants';
import {Person} from '../../person/domain/person';
import {newFullName} from '../../../infrastructure/util/full-name/full-name.factory';

export const trip = (): TripOverview => {
  return <TripOverview> {
    name: 'Skivakantie 2018',
    persons: [person(), otherPerson()],
    balances: [],
    expenses: [expense(), otherExpense(), anotherExpense()]
  };
};

export const otherTrip = (): TripOverview => {
  return <TripOverview> {
    name: 'Mannenweekend 2019',
    persons: [<Person> {
      personId: 'c5fd75f9-5424-40cf-9bb2-808724ca7e7b',
      fullName: newFullName('Jeroen', 'Stroobants'),
      email: 'jeroenstroobants@gmail.com'
    }, <Person> {
      personId: 'c5fd75f9-5424-40cf-9bb2-808724ca7e7b',
      fullName: newFullName('Kevin', 'Van Vaerenbergh'),
      email: 'jeroenstroobants@gmail.com'
    }, <Person> {
      personId: '542c5a19-34ed-41a3-ace5-8fc058765c83',
      fullName: newFullName('Christophe', 'Bievelez'),
      email: 'jeroenstroobants@gmail.com'
    }, <Person> {
      personId: '542c5a19-34ed-41a3-ace5-8fc058765c83',
      fullName: newFullName('Thomas', 'Boonen')
    }, <Person> {
      personId: '542c5a19-34ed-41a3-ace5-8fc058765c83',
      fullName: newFullName('Maarten', 'Devillé'),
      email: 'jeroenstroobants@gmail.com'
    }],
    balances: [],
    expenses: [expense(), otherExpense(), anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense(),
      anotherExpense()
    ]
  };
};
export const anotherTrip = (): TripOverview => {
  return <TripOverview> {
    name: 'London 2017',
    persons: [person(), otherPerson()],
    balances: [],
    expenses: [expense(), otherExpense(), anotherExpense()]
  };
};
