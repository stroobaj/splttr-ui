import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {TripOverview} from '../../trip/domain/trip-overview';
import {ExpenseService} from '../domain/expense.service';
import {ActivatedRoute} from '@angular/router';
import {CreateExpense} from '../domain/create-expense';
import {FullName, getDisplayName} from '../../../infrastructure/util/full-name/full-name';
import {momentFormatSpec, yearMonthDayMask} from '../../../infrastructure/util/date/date-utils';
import * as moment from 'moment';
import {euroAmountSymbol, numberMask} from '../../../infrastructure/util/amount/amount-utils';
import {Person} from '../../person/domain/person';

@Component({
  selector: 'app-create-expense-modal',
  template: `
    <app-modal [title]="'Create Expense'"
               [hasClose]="true">
      <form [formGroup]="createExpenseForm"
            (ngSubmit)="createExpense()"
            novalidate>

        <mat-form-field class="full-width-input">
          <input matInput
                 placeholder="Description"
                 formControlName="description"
                 required>
          <mat-error *ngIf="description.invalid && (description.dirty || description.touched)">Description is required</mat-error>
        </mat-form-field>

        <mat-form-field class="full-width-input">
          <input matInput
                 placeholder="Amount"
                 [textMask]="{mask: getEuroAmountMask(), keepCharPositions:false}"
                 formControlName="amount"
                 required>
          <mat-error *ngIf="amount.invalid && (amount.dirty || amount.touched)">Amount is required</mat-error>
        </mat-form-field>

        <mat-form-field class="full-width-input">
          <input matInput
                 placeholder="Date"
                 [textMask]="{mask: getYearMonthDayMask(), keepCharPositions:false}"
                 formControlName="date"
                 required>
          <mat-error *ngIf="date.errors && (date.dirty || date.touched)">
            <mat-error *ngIf="date.errors['required']">Date is required</mat-error>
            <mat-error *ngIf="!(date.errors['required']) && date.errors['invalidDate']">Not a valid date.</mat-error>
          </mat-error>
        </mat-form-field>

        <mat-form-field class="full-width-input">
          <mat-select placeholder="Paid by"
                      formControlName="payerId"
                      required>
            <mat-option *ngFor="let person of trip.persons"
                        [value]="person.personId">{{getPersonDisplayName(person.fullName)}}
            </mat-option>
          </mat-select>
          <mat-error *ngIf="payer.invalid && (payer.dirty || payer.touched)">Paid by is required</mat-error>
        </mat-form-field>

        <!-- CHECKBOXES PERSONS -->
        <div>Split with:</div>
        <div formArrayName="persons"
             *ngFor="let person of createExpenseForm.controls['persons'].controls; let i = index">
          <mat-checkbox type="checkbox"
                        [formControlName]="i">
            {{getPersonDisplayName(allPersons[i]?.fullName)}}
          </mat-checkbox>
        </div>
        <div class="app-invalid-feedback" *ngIf="createExpenseForm.controls['persons'].invalid 
          && (createExpenseForm.controls['persons'].dirty || createExpenseForm.controls['persons'].touched)">
          <span *ngIf="createExpenseForm.controls['persons'].invalid">At least one person must be selected</span>
        </div>

        <button mat-raised-button
                type="submit"
                [disabled]="!createExpenseForm.valid">Submit
        </button>

      </form>
    </app-modal>
  `
})
export class CreateExpenseModalComponent implements OnInit {

  trip: TripOverview;

  createExpenseForm: FormGroup;
  description: FormControl;
  amount: FormControl;
  date: FormControl;
  payer: FormControl;
  persons: FormArray = new FormArray([]);

  allPersons: Array<Person> = [];

  @Output() onExpenseCreated: EventEmitter<any> = new EventEmitter<any>();

  constructor(private expenseService: ExpenseService,
              private ngbActiveModal: NgbActiveModal,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.createFormControls();
    this.createForm();
    this.setPersons();
  }

  createFormControls = (): void => {
    this.description = this.formBuilder.control('', Validators.required);
    this.amount = this.formBuilder.control('', Validators.required);
    this.date = this.formBuilder.control('', [
      Validators.required,
      dateValidator
    ]);
    this.payer = this.formBuilder.control('', Validators.required);
  };

  createForm = (): void => {
    this.createExpenseForm = this.formBuilder.group({
      description: this.description,
      amount: this.amount,
      date: this.date,
      payerId: this.payer,
      partakers: this.persons
    });
  };

  createExpense = (): void => {
    const selectedPersonIds: string[] = this.createExpenseForm.value.persons
      .map((v, i) => v ? this.allPersons[i].personId : null)
      .filter(v => v !== null);

    const createExpense: CreateExpense = this.deepCopy(this.createExpenseForm.value);
    createExpense.tripId = this.trip.tripId;
    createExpense.date = this.formatDate(createExpense.date);
    createExpense.amount = this.formatAmount(createExpense.amount);
    createExpense.partakers = selectedPersonIds;

    this.expenseService.createExpense(createExpense).subscribe(() => {
      this.onExpenseCreated.emit();
    });
    this.closeModal();
  };

  setPersons = (): void => {
    this.trip.persons.forEach((person: Person) => {
      this.allPersons.push(person);
    });
    this.updateFormValues();
  };

  updateFormValues = (): void => {
    const controls: Array<FormControl> = this.allPersons.map(c => new FormControl(false));
    this.createExpenseForm.setControl('persons', this.formBuilder.array(controls, minSelectedCheckboxes(1)));
  };

  getPersonDisplayName = (fullName: FullName): string => {
    return getDisplayName(fullName);
  };

  closeModal = (): void => {
    this.ngbActiveModal.close();
  };

  deepCopy = (value: any): any => {
    return JSON.parse(JSON.stringify(value));
  };

  formatDate = (date: string): string => {
    return moment(date, momentFormatSpec, true).format('YYYY-MM-DD');
  };

  formatAmount = (amount: string): string => {
    return amount
      .replace(euroAmountSymbol + ' ', '')
      .replace(/\./g, '')
      .replace('\,', '\.').trim();
  };

  getYearMonthDayMask = (): Array<string | RegExp> => {
    return yearMonthDayMask;
  };

  getEuroAmountMask = (): Array<string | RegExp> => {
    return numberMask;
  };

}

function dateValidator(formControl: FormControl) {
  const date: string = formControl.value;
  const dateStructure: RegExp = new RegExp('^\\d{2}/\\d{2}/\\d{4}$');

  if (!dateStructure.test(date)) {
    // while typing the date
    return null;
  } else if (dateStructure.test(date)) {
    return moment(date, 'DD/MM/YYYY', true).isValid() === true ? null : {invalidDate: true};
  }
}

function minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      .map(control => control.value)
      .reduce((prev, next) => next ? prev + next : prev, 0);

    return totalSelected >= min ? null : {required: true};
  };
  return validator;
}
