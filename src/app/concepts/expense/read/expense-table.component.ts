import {Component, Input, OnInit} from '@angular/core';
import {ExpenseInfo, ExtendedExpenseInfo} from '../domain/expense-info';
import {momentFormatSpec} from '../../../infrastructure/util/date/date-utils';
import {FullName, getDisplayName} from '../../../infrastructure/util/full-name/full-name';
import * as moment from 'moment';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {TripService} from '../../trip/domain/trip.service';

@Component({
  selector: 'app-expense-table',
  styleUrls: ['./expense-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  template: `
    <div *ngIf="!extendedExpenses || extendedExpenses.length === 0">
      No expenses created yet.
    </div>

    <table *ngIf="extendedExpenses && extendedExpenses.length > 0"
           [dataSource]="extendedExpenses"
           mat-table
           multiTemplateDataRows
           class="mat-elevation-z3">
      <ng-container matColumnDef="description">
        <th mat-header-cell *matHeaderCellDef>Description</th>
        <td mat-cell *matCellDef="let extendedExpense">{{extendedExpense.description}}</td>
      </ng-container>

      <ng-container matColumnDef="date">
        <th mat-header-cell *matHeaderCellDef> Date</th>
        <td mat-cell *matCellDef="let extendedExpense">{{displayDate(extendedExpense.date)}}</td>
      </ng-container>

      <ng-container matColumnDef="amount">
        <th mat-header-cell *matHeaderCellDef>Amount</th>
        <td mat-cell *matCellDef="let extendedExpense">{{extendedExpense.amount | euroCurrency}} </td>
      </ng-container>

      <ng-container matColumnDef="paid">
        <th mat-header-cell *matHeaderCellDef>Paid</th>
        <td mat-cell *matCellDef="let extendedExpense">{{getPersonDisplayName(extendedExpense.payer.fullName)}}</td>
      </ng-container>

      <!-- Expanded Content Column -->
      <ng-container matColumnDef="expandedDetail">
        <td mat-cell *matCellDef="let extendedExpense" [attr.colspan]="displayedColumns.length">
          <div class="example-element-detail"
               [@detailExpand]="extendedExpense == expandedElement ? 'expanded' : 'collapsed'">
            <mat-chip-list style="padding: 10px 0 10px 0;">
              <div *ngFor="let allocation of extendedExpense.expenseAllocations">
                <mat-chip [ngStyle]="{background: isPositiveBalance(allocation.amount) ? '#52EB7C' : '#eb0f0f'}">
                  {{getPersonDisplayName(allocation.person.fullName) + ': ' + (allocation.amount | euroCurrency)}}
                </mat-chip>
              </div>
            </mat-chip-list>
          </div>
        </td>
      </ng-container>

      <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
      <tr mat-row *matRowDef="let row; columns: displayedColumns;"
          class="example-element-row"
          [class.example-expanded-row]="expandedElement === row"
          (click)="expandedElement = expandedElement === row ? null : row;">
      </tr>
      <tr mat-row *matRowDef="let row; columns: ['expandedDetail']" class="example-detail-row"></tr>
    </table>
  `
})
export class ExpenseTableComponent implements OnInit {

  @Input()
  expenses: ExpenseInfo[];
  @Input()
  tripId: string;

  extendedExpenses: ExtendedExpenseInfo[];


  expandedElement: ExpenseInfo | null;
  displayedColumns: string[] = ['description', 'date', 'amount', 'paid'];

  constructor(private tripService: TripService) {
  }

  ngOnInit(): void {
    this.tripService.getExtendedExpenses(this.tripId).subscribe((extendedExpenses: ExtendedExpenseInfo[]) => {
      this.extendedExpenses = extendedExpenses;
    });
  }

  displayDate = (date: string): string => {
    return moment(date, momentFormatSpec).format('DD/MM/YYYY');
  };

  getPersonDisplayName = (fullName: FullName): string => {
    return getDisplayName(fullName);
  };

  isPositiveBalance = (balance: string): boolean => {
    return Math.sign(parseFloat(balance)) >= 0;
  };

  // toggleRow = (): any => {
  //   console.log('TOGGLED');
  //   this.expenseService.getExpenseAllocation('').subscribe(expenseAllocation => {
  //     this.expandedElement = expenseAllocation;
  //     console.log('expandedElement', JSON.stringify(this.expandedElement));
  //   });
  // };

}
