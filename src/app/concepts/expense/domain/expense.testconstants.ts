import {ExpenseInfo} from './expense-info';
import {anotherPerson, otherPerson, person} from '../../person/domain/person.testconstants';

export const expense = (): ExpenseInfo => {
  return <ExpenseInfo> {
    payer: person(),
    date: '25-01-2019',
    description: 'Drank',
    amount: '25.99'
  };
};

export const otherExpense = (): ExpenseInfo => {
  return <ExpenseInfo> {
    payer: otherPerson(),
    date: '25-05-2019',
    description: 'Eten',
    amount: '36.50'
  };
};

export const anotherExpense = (): ExpenseInfo => {
  return <ExpenseInfo> {
    payer: anotherPerson(),
    date: '25-08-2019',
    description: 'Hotel',
    amount: '1250.37'
  };
};
