export interface CreateExpense {
  tripId: string;
  payerId: string;
  description: string;
  date: string;
  amount: string;
  partakers: string[];
}
