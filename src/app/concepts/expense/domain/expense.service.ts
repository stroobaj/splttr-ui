import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CreateExpense} from './create-expense';
import {Observable} from 'rxjs';

@Injectable()
export class ExpenseService {

  EXPENSE_URL: string = '/api/expense';

  constructor(private http: HttpClient) {
  }

  createExpense = (createExpense: CreateExpense): Observable<Object> => {
    return this.http.post(this.EXPENSE_URL, createExpense);
  };

}
