import {Person} from '../../person/domain/person';

export interface ExpenseInfo {
  payer: Person;
  description: string;
  date: string;
  amount: string;
}

export interface ExtendedExpenseInfo extends ExpenseInfo {
  expenseAllocations: ExpenseAllocation[];
}

export interface ExpenseAllocation {
  person: Person;
  amount: string;
}
