import {CreateExpense} from './create-expense';

export function newCreateExpense(): CreateExpense {
  return <CreateExpense>{
    tripId: '',
    payerId: '',
    description: '',
    date: '',
    amount: '',
    partakers: ['']
  };
}
