import {Component, Input} from '@angular/core';
import {FullName, getDisplayName} from '../../../infrastructure/util/full-name/full-name';
import {PersonBalance} from '../domain/person-balance';

@Component({
  selector: 'app-person-balance',
  template: `
    <mat-card>
      <mat-card-content>
        <div>{{getPersonDisplayName(personBalance.person.fullName)}}</div>
        <div *ngIf="isPositiveBalance(personBalance.balance)"
             class="positive-balance">gets back {{personBalance.balance | euroCurrency}}
        </div>
        <div *ngIf="!isPositiveBalance(personBalance.balance)"
             class="negative-balance">owes {{personBalance.balance | euroCurrency}}
        </div>
      </mat-card-content>
    </mat-card>
  `
})
export class PersonBalanceComponent {

  @Input()
  personBalance: PersonBalance;

  constructor() {
  }

  isPositiveBalance = (balance: string): boolean => {
    return Math.sign(parseFloat(balance)) >= 0;
  };

  getPersonDisplayName = (fullName: FullName): string => {
    return getDisplayName(fullName);
  };

}
