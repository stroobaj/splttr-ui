import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PersonBalance, UserBalance} from './person-balance';

@Injectable()
export class BalanceService {

  private BALANCE_URL: string = '/api/balance/';

  constructor(private http: HttpClient) {
  }

  getTripBalances = (tripId: string): Observable<Array<PersonBalance>> => {
    return this.http.get<Array<PersonBalance>>(this.BALANCE_URL + tripId);
  };

  getPersonBalances = (): Observable<Array<UserBalance>> => {
    return this.http.get<Array<UserBalance>>(this.BALANCE_URL);
  };

  getLastEditedUserBalances = (): Observable<Array<UserBalance>> => {
    return this.http.get<Array<UserBalance>>(this.BALANCE_URL + 'recent');
  };
}
