import {Person} from '../../person/domain/person';

export interface PersonBalance {
  person: Person;
  balance: string;
  advance: string;
  borrowed: string;
}

export interface UserBalance {
  tripId: string;
  tripName: string;
  balance: string;
  LastEditDateTime: string;
}

