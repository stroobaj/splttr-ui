import {FullName} from '../../../infrastructure/util/full-name/full-name';

export interface Person {
  personId: string;
  fullName: FullName;
  email: string;
}

export function getDisplayName(fullName: FullName): string {
  return fullName.firstName + ' ' + fullName.lastName;
}
