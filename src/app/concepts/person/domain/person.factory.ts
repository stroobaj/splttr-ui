import {PersonInfo} from './person-info';

export function newPersonInfo(): PersonInfo {
  return <PersonInfo>{
    firstName: '',
    lastName: '',
    email: ''
  };
}
