import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Person} from './person';
import {Observable} from 'rxjs';
import {PersonInfo} from './person-info';

@Injectable()
export class PersonService {

  PERSON_URL: string = '/api/person';

  constructor(private http: HttpClient) {
  }

  getAllPersons = (): Observable<Array<Person>> => {
    return this.http.get<Array<Person>>(this.PERSON_URL);
  };

  createPerson = (personInfo: PersonInfo): Observable<Object> => {
    return this.http.post(this.PERSON_URL, personInfo);
  };

  editPerson = (personId: string, personInfo: PersonInfo): Observable<Object> => {
    return this.http.post(this.PERSON_URL + '/' + personId + '/edit', personInfo);
  };
}
