import {Person} from './person';
import {newFullName} from '../../../infrastructure/util/full-name/full-name.factory';

export const person = (): Person => {
  return <Person> {
    personId: 'c5fd75f9-5424-40cf-9bb2-808724ca7e7b',
    fullName: newFullName('Jeroen', 'Stroobants'),
    email: 'jeroenstroobants@gmail.com'
  };
};

export const otherPerson = (): Person => {
  return <Person> {
    personId: '542c5a19-34ed-41a3-ace5-8fc058765c83',
    fullName: newFullName('Sarah', 'Smagghe'),
    email: 'sarahsmagghe@gmail.com'
  };
};

export const anotherPerson = (): Person => {
  return <Person> {
    personId: '542c5a19-34ed-41a3-ace5-8fc058765c83',
    fullName: newFullName('Maarten', 'Devillé'),
    email: 'maartendeville@gmail.com'
  };
};
