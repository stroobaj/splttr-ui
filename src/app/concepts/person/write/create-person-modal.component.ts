import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {PersonService} from '../domain/person.service';
import {PersonInfo} from '../domain/person-info';

@Component({
  selector: 'app-create-person-modal',
  template: `
    <app-modal [title]="'Create Person'"
               [hasClose]="true">
      <form [formGroup]="createPersonForm"
            (ngSubmit)="createPerson()"
            novalidate>
        <mat-form-field class="full-width-input">
          <input matInput
                 placeholder="First name"
                 formControlName="firstName"
                 required>
          <mat-error *ngIf="firstName.invalid && (firstName.dirty || firstName.touched)">First name is required</mat-error>
        </mat-form-field>

        <mat-form-field class="full-width-input">
          <input matInput
                 placeholder="Last name"
                 formControlName="lastName"
                 required>
          <mat-error *ngIf="lastName.invalid && (lastName.dirty || lastName.touched)">Last name is required</mat-error>
        </mat-form-field>


        <mat-form-field class="full-width-input">
          <input matInput
                 placeholder="Email"
                 formControlName="email"
                 required>
          <mat-error *ngIf="email.errors && (email.dirty || email.touched)">
            <mat-error *ngIf="email.errors['required']">Email is required</mat-error>
            <mat-error *ngIf="!(email.errors['required']) && email.errors['email']">Not a valid email address.</mat-error>
          </mat-error>
        </mat-form-field>
        
        <button mat-raised-button
                type="submit"
                [disabled]="!createPersonForm.valid">Submit
        </button>
      </form>
    </app-modal>
  `
})

export class CreatePersonModalComponent implements OnInit {

  @Output() onPersonCreated: EventEmitter<any> = new EventEmitter<any>();

  createPersonForm: FormGroup;
  firstName: FormControl;
  lastName: FormControl;
  email: FormControl;

  constructor(private personService: PersonService,
              private ngbActiveModal: NgbActiveModal,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.createFormControls();
    this.createForm();
  }

  createFormControls = (): void => {
    this.firstName = this.formBuilder.control('', Validators.required);
    this.lastName = this.formBuilder.control('', Validators.required);
    this.email = this.formBuilder.control('', [
      Validators.required,
      Validators.email]);
  };

  createForm = (): void => {
    this.createPersonForm = this.formBuilder.group({
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
    });
  };

  createPerson = (): void => {
    const personInfo: PersonInfo = JSON.parse(JSON.stringify(this.createPersonForm.value));
    this.personService.createPerson(personInfo).subscribe(() => {
      this.onPersonCreated.emit();
    });
    this.closeModal();
  };

  closeModal = (): void => {
    this.ngbActiveModal.close();
  };

}
