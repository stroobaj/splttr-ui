import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {PersonService} from '../domain/person.service';
import {Person} from '../domain/person';
import {PersonInfo} from '../domain/person-info';

@Component({
  selector: 'app-edit-person-modal',
  template: `
    <app-modal [title]="'Edit Person'"
               [hasClose]="true">
      <form [formGroup]="editPersonForm"
            (ngSubmit)="editPerson()"
            novalidate>
        <mat-form-field class="full-width-input">
          <input matInput
                 placeholder="First name"
                 formControlName="firstName"
                 required>
          <mat-error *ngIf="firstName.invalid && (firstName.dirty || firstName.touched)">First name is required</mat-error>
        </mat-form-field>

        <mat-form-field class="full-width-input">
          <input matInput
                 placeholder="Last name"
                 formControlName="lastName"
                 required>
          <mat-error *ngIf="lastName.invalid && (lastName.dirty || lastName.touched)">Last name is required</mat-error>
        </mat-form-field>


        <mat-form-field class="full-width-input">
          <input matInput
                 placeholder="Email"
                 formControlName="email"
                 required>
          <mat-error *ngIf="email.errors && (email.dirty || email.touched)">
            <mat-error *ngIf="email.errors['required']">Email is required</mat-error>
            <mat-error *ngIf="!(email.errors['required']) && email.errors['email']">Not a valid email address.</mat-error>
          </mat-error>
        </mat-form-field>

        <button mat-raised-button
                type="submit"
                [disabled]="!editPersonForm.valid">Submit
        </button>
      </form>
    </app-modal>
  `
})

export class EditPersonModalComponent implements OnInit {

  @Output() onPersonEdited: EventEmitter<any> = new EventEmitter<any>();

  person: Person;
  editPersonForm: FormGroup;
  firstName: FormControl;
  lastName: FormControl;
  email: FormControl;

  constructor(private personService: PersonService,
              private ngbActiveModal: NgbActiveModal,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.createFormControls();
    this.createForm();
  }

  createFormControls = (): void => {
    this.firstName = this.formBuilder.control(this.person.fullName.firstName, Validators.required);
    this.lastName = this.formBuilder.control(this.person.fullName.lastName, Validators.required);
    this.email = this.formBuilder.control(this.person.email, [
      Validators.required,
      Validators.email]);
  };

  createForm = (): void => {
    this.editPersonForm = this.formBuilder.group({
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
    });
  };

  editPerson = (): void => {
    const personInfo: PersonInfo = JSON.parse(JSON.stringify(this.editPersonForm.value));
    this.personService.editPerson(this.person.personId, personInfo).subscribe(() => {
      this.onPersonEdited.emit();
    });
    this.closeModal();
  };

  closeModal = (): void => {
    this.ngbActiveModal.close();
  };

}
