import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {PersonService} from '../domain/person.service';
import {Person} from '../domain/person';
import {RunningRequests} from '../../../shared/commons/domain/running-requests';
import {CreatePersonModalComponent} from '../write/create-person-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TitleService} from '../../../shared/commons/title.service';

@Component({
  selector: 'app-person-overview',
  styleUrls: ['./person-overview.component.scss'],

  template: `
    <div class="container">
      <!-- MODAL -->
      <button #createPersonButton
              mat-raised-button
              type="button"
              class="app-create-button"
              (click)="openCreatePersonModal()">Create Person
      </button>

      <!-- CARDS -->
      <div *ngIf="!runningRequests.isRequestForAllPersonsRunning()">
        <div *ngIf="!persons || persons.length === 0">
          No persons found.
        </div>

        <div *ngIf="persons && persons.length > 0"
             class="app-person-cards">
          <app-person-information-card *ngFor="let person of persons"
                                       [person]="person"
                                       (onPersonEdited)="loadPersons()">
          </app-person-information-card>
        </div>
      </div>
    </div>
  `
})
export class PersonOverviewComponent implements OnInit {

  @ViewChild('createPersonButton')
  private createPersonButton: ElementRef;

  persons: Person[];
  runningRequests: RunningRequests = new RunningRequests();

  constructor(private personService: PersonService,
              private modalService: NgbModal,
              private titleService: TitleService) {
  }

  ngOnInit(): void {
    this.loadPersons();
  }

  loadPersons = (): void => {
    this.runningRequests.startRequestForPersons();
    this.personService.getAllPersons().subscribe(persons => {
        this.persons = persons;
        this.titleService.setTitle(['FRIENDS']);
      },
      (error) => console.log('Error: ', error),
      () => this.runningRequests.allPersonsRetrieved());
  };

  openCreatePersonModal = (): void => {
    const ngbModalRef = this.modalService.open(CreatePersonModalComponent);
    ngbModalRef.componentInstance.onPersonCreated.subscribe(() => {
      this.loadPersons();
    });
    // TODO: check better option - above of below.
    ngbModalRef.result
      .then(() => {
        this.createPersonButton['_elementRef'].nativeElement.classList.remove('cdk-program-focused');
        console.log('SUBMIT');
      })
      .catch(() => {
        this.createPersonButton['_elementRef'].nativeElement.classList.remove('cdk-program-focused');
        console.log('CLOSE');
      });
  };

}
