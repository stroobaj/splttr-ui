import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Person} from '../domain/person';
import {FullName, getDisplayName} from '../../../infrastructure/util/full-name/full-name';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {EditPersonModalComponent} from '../write/edit-person-modal.component';
import {AuthService} from '../../../security/auth/auth.service';

@Component({
  selector: 'app-person-information-card',
  template: `
    <mat-card class="app-person-card">
      <mat-card-title class="app-person-title">{{getPersonDisplayName(person.fullName)}}</mat-card-title>
      <mat-card-content>{{person.email}}</mat-card-content>
      <div *ngIf="isPrincipal()">
        <mat-icon style="font-size: 20px">person</mat-icon>
      </div>
      <div *ngIf="!isPrincipal()">
        <mat-icon style="font-size: 20px"
                  (click)="openEditPersonModal()">edit
        </mat-icon>
        <mat-icon style="font-size: 20px"
                  (click)="openDeletePersonModal()">deleted
        </mat-icon>
      </div>
    </mat-card>
  `
})
export class PersonInformationCardComponent {

  @Input()
  person: Person;

  @Output()
  onPersonEdited: EventEmitter<any> = new EventEmitter<any>();

  constructor(private modalService: NgbModal,
              private authService: AuthService) {
  }

  getPersonDisplayName = (fullName: FullName): string => {
    return getDisplayName(fullName);
  };

  openEditPersonModal = (): void => {
    const ngbModalRef = this.modalService.open(EditPersonModalComponent);
    ngbModalRef.componentInstance.person = this.person;
    ngbModalRef.componentInstance.onPersonEdited.subscribe(() => {
      this.onPersonEdited.emit();
    });
    // TODO: check better option - above of below.
    // ngbModalRef.result
    //   .then(() => console.log('SUBMIT'))
    //   .catch(() => console.log('CLOSE'));
    console.log('edit');
  };

  openDeletePersonModal = (): void => {
    console.log('delete');
  };

  isPrincipal = (): boolean => {
    return this.person.personId === this.authService.getPrincipalId();
  };


}
