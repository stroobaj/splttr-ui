import {Component} from '@angular/core';

@Component({
  selector: 'app-sub-toolbar',
  template: `
    <mat-toolbar color="secondary">
      <mat-toolbar-row>
        <span>Custom actions</span>
      </mat-toolbar-row>
    </mat-toolbar>
  `
})

export class SubToolbarComponent {
}
