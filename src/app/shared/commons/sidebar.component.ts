import {Component} from '@angular/core';
import {AuthService} from '../../security/auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sidebar',
  template: `
    <mat-toolbar color="accent" style="min-height: 40px">
      <mat-toolbar-row style="height: 40px">
        <button mat-button [matMenuTriggerFor]="menu">
          <mat-icon>menu</mat-icon>
        </button>
        <mat-menu #menu="matMenu">
          <button mat-menu-item routerLink="/home">Home</button>
          <button mat-menu-item routerLink="/trip">Trips</button>
          <button mat-menu-item routerLink="/friend">Friends</button>
        </mat-menu>
        <app-title></app-title>
        <span style="flex: 1 1 auto;"></span>
        <button mat-button class="logout-icon">
          <mat-icon (click)="logout()">exit_to_app</mat-icon>
        </button>
      </mat-toolbar-row>
    </mat-toolbar>
  `
})
export class SidebarComponent {

  constructor(private authService: AuthService,
              private router: Router) {
  }

  logout = (): void => {
    console.log('LOGGING OUT...');
    this.authService.signOut();
    this.router.navigate(['login']);
  };

}
