import {BehaviorSubject} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable()
export class TitleService {
  public title = new BehaviorSubject('');

  constructor() {
  }

  setTitle(titles: string[]) {
    const fullTitle = titles
      .map((title: string) => title.toUpperCase())
      .join(' \\ ');
    this.title.next(fullTitle);
  }

}
