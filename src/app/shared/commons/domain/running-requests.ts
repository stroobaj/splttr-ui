export class RunningRequests {

  private requestForAllPersonsIsRunning: boolean = false;
  private requestForAllTripsIsRunning: boolean = false;
  private requestForAllExpensesIsRunning: boolean = false;

  public startRequestForPersons = (): void => {
    this.requestForAllPersonsIsRunning = true;
  };

  public allPersonsRetrieved = (): void => {
    this.requestForAllPersonsIsRunning = false;
  };

  public isRequestForAllPersonsRunning = (): boolean => {
    return this.requestForAllPersonsIsRunning;
  };

  public startRequestForTrips = (): void => {
    this.requestForAllTripsIsRunning = true;
  };

  public allTripsRetrieved = (): void => {
    this.requestForAllTripsIsRunning = false;
  };

  public isRequestForAllTripsRunning = (): boolean => {
    return this.requestForAllTripsIsRunning;
  };

  public startRequestForExpenses = (): void => {
    this.requestForAllExpensesIsRunning = true;
  };

  public allExpensesRetrieved = (): void => {
    this.requestForAllExpensesIsRunning = false;
  };

  public isRequestForAllExpensesRunning = (): boolean => {
    return this.requestForAllExpensesIsRunning;
  };

}
