import {Component, OnInit} from '@angular/core';
import {TitleService} from './title.service';

@Component({
  selector: 'app-title',
  template: `
    <div>
      <span class="app-title">{{title}}</span>
    </div>
  `
})
export class TitleComponent implements OnInit {

  title = '';

  constructor(private titleService: TitleService) {
  }

  ngOnInit() {
    this.titleService.title.subscribe(title => {
      this.title = title;
    });
  }

}

