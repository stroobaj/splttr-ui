import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal',
  template: `    
    <mat-card>
      <mat-card-title>{{title}}
        <button *ngIf="hasClose"
                (click)="closeModal()"
                type="button"
                class="close">
          <span>&times;</span>
        </button>
      </mat-card-title>

      <mat-card-content>
        <ng-content></ng-content>
      </mat-card-content>
    </mat-card>
  `
})
export class ModalComponent {

  @Input() title: string = ``;
  @Input() hasClose: boolean = false;

  @Output() onClose: EventEmitter<void> = new EventEmitter();

  constructor(public activeModal: NgbActiveModal) {
  }

  closeModal = (): void => {
    this.onClose.emit();
    this.activeModal.dismiss();
  };

}
