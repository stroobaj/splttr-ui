import {Component} from '@angular/core';

@Component({
  selector: 'app-toolbar',
  template: `
    <mat-toolbar color="primary">
      <mat-toolbar-row>
        <span class="mx-auto">SPLTTR</span>
        <!--<span class="toolbar-spacer"></span>-->
        <!--<button mat-raised-button-->
        <!--class="logout-icon">-->
        <!--<mat-icon (click)="logout()">exit_to_app</mat-icon>-->
        <!--</button>-->
      </mat-toolbar-row>
    </mat-toolbar>
  `
})
export class ToolbarComponent {

  // constructor(private authService: AuthService,
  //             private router: Router) {
  // }
  //
  // logout = (): void => {
  //   console.log('LOGGING OUT...');
  //   this.authService.signOut();
  //   this.router.navigate(['login']);
  // };

}
