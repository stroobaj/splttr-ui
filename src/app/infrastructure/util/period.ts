export interface Period {
  beginDate: string;
  endDate: string;
}
