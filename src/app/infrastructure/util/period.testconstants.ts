import {Period} from './period';

export const period = (): Period => {
  return <Period> {
    beginDate: '23-03-2018',
    endDate: '30-03-2018'
  };
};

export const otherPeriod = (): Period => {
  return <Period> {
    beginDate: '25-05-2019',
    endDate: '29-05-2018'
  };
};
