export class DateUtilsService {

  public displayDate = (date: string): Date => {
    return new Date(date);
  };
}

export const DateUtils: DateUtilsService = new DateUtilsService();

export const yearMonthDaySeparator = '-';
export const yearMonthDayViewSeparator = '/';
export const yearMonthDayPlaceholder: string = 'dd' + yearMonthDayViewSeparator + 'mm' + yearMonthDayViewSeparator + 'yyyy';
export const yearMonthDayMask: Array<string | RegExp> = [
  /\d/, /\d/, yearMonthDayViewSeparator,
  /\d/, /\d/, yearMonthDayViewSeparator,
  /\d/, /\d/, /\d/, /\d/
];

export const momentFormatSpec = [
  'YYYY' + yearMonthDaySeparator + 'MM' + yearMonthDaySeparator + 'DD',
  'YYYY' + yearMonthDaySeparator + 'M' + yearMonthDaySeparator + 'DD',
  'YYYY' + yearMonthDaySeparator + 'MM' + yearMonthDaySeparator + 'D',
  'YYYY' + yearMonthDaySeparator + 'M' + yearMonthDaySeparator + 'D',
  'DD' + yearMonthDayViewSeparator + 'MM' + yearMonthDayViewSeparator + 'YYYY',
  'DD' + yearMonthDayViewSeparator + 'M' + yearMonthDayViewSeparator + 'YYYY',
  'D' + yearMonthDayViewSeparator + 'MM' + yearMonthDayViewSeparator + 'YYYY',
  'D' + yearMonthDayViewSeparator + 'M' + yearMonthDayViewSeparator + 'YYYY'
];
