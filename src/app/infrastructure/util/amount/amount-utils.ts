import createNumberMask from 'text-mask-addons/dist/createNumberMask'

export const euroAmounthSeparator = '.';
export const euroAmountSymbol = '€';
export const euroAmountPlaceholder: string = euroAmountSymbol + ' 0' + euroAmounthSeparator + '00';
export const euroAmountMask: Array<string | RegExp> = [
  /\d/, /\d/, euroAmounthSeparator,
  /\d/, /\d/
];
export const euroAmountMask2: Array<string | RegExp> = [
  new RegExp('\\d+(\\.\\d{1,2})?')
];

export const numberMask = createNumberMask({
  prefix: euroAmountSymbol + ' ',
  includeThousandsSeparator: true,
  thousandsSeparatorSymbol: '.',
  allowDecimal: true,
  decimalSymbol: ',',
  decimalLimit: 2,
  allowNegative: false
  // suffix: 'euroAmountSymbol' // This will put the dollar sign at the end, with a space.
});
