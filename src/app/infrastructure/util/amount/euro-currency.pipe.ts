import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'euroCurrency'
})
export class EuroCurrencyPipe implements PipeTransform {

  transform(value: number | string): string {
    if (value === undefined || value === null) {
      return '-';
    } else if (typeof value === 'string') {
      return this.formatMoney(parseFloat(value));
    } else {
      return this.formatMoney(value);
    }
  }

  // remark: we use the decimal style here instead of the currency style.
  // this is due to the different types of browsers that each places the currency symbol at another place (front or back)
  // we want the symbol always in each browser placed before the amount
  formatMoney = (value: number): string => {
    const format = new Intl.NumberFormat(['nl-BE'],
      {style: 'decimal', minimumFractionDigits: 2, maximumFractionDigits: 2, useGrouping: true}
    );
    return '€ ' + format.format(Number(value));
  };

}
