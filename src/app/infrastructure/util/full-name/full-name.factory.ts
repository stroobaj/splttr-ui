import {FullName} from './full-name';

export function emptyFullName() {
  return <FullName> {
    firstName: '',
    lastName: ''
  };
}

export function newFullName(firstName: string, lastName: string) {
  return <FullName> {
    firstName: firstName,
    lastName: lastName
  };
}
