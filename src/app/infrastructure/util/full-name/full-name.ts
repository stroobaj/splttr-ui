export interface FullName {
  lastName: string;
  firstName: string;
}

export function getDisplayName(fullName: FullName): string {
  return fullName.firstName + ' ' + fullName.lastName;
}
