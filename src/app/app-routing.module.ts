import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TestOverviewComponent} from './TEST/test-overview.component';
import {TripOverviewComponent} from './concepts/trip/read/trip-overview.component';
import {TripDetailComponent} from './concepts/trip/read/trip-detail.component';
import {PersonOverviewComponent} from './concepts/person/read/person-overview.component';
import {AuthenticationGuard} from './security/auth/authentication.guard';
import {LoginSucceededComponent} from './home/login-succeeded.component';
import {LoginComponent} from './security/login.component';
import {SignUpMatComponent} from './security/sign-up-mat.component';
import {UserHomeComponent} from './home/user-home.component';

const routes: Routes = [

  // home route protected by auth guard
  {
    path: '',
    component: LoginSucceededComponent,
    canActivate: [AuthenticationGuard],
    children: [
      {path: 'home', component: UserHomeComponent},
      {path: 'trip', component: TripOverviewComponent},
      {path: 'trip/:tripId', component: TripDetailComponent},
      {path: 'friend', component: PersonOverviewComponent},
      {path: 'test', component: TestOverviewComponent},
    ]
  },
  {path: 'signup', component: SignUpMatComponent},

  // route when not authenticated
  {path: 'login', component: LoginComponent},
  // otherwise redirect to home
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
